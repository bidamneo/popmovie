package com.example.odom.popmovie.video;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class VideoContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<VideoItem> ITEMS = new ArrayList<VideoItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, VideoItem> ITEM_MAP = new HashMap<String, VideoItem>();

    private static final int COUNT = 25;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(VideoItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static VideoItem createDummyItem(int position) {
        ArrayList<Trailer> trailersList = new ArrayList<Trailer>();

        for(int i=0; i<3; i++) {
            trailersList.add(i, new Trailer(i, "https://www.youtube.com/watch?v=dKrVegVI0Us"));
        }
        return new VideoItem(String.valueOf(position), "https://img1.homeenter.com/picHE/JPG340/Col/Colonia-DVDFilm-0101201.jpg", makeDetails(position), trailersList);
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class VideoItem {
        public final String id;
        public final String imageUrl;
        public final String content;
        public final ArrayList<Trailer> trailers;

        public VideoItem(String id, String imageUrl, String content, ArrayList<Trailer> trailers) {
            this.id = id;
            this.imageUrl = imageUrl;
            this.content = content;
            this.trailers = trailers;
        }

        @Override
        public String toString() {
            return content;
        }
    }

    public static class Trailer {
        public final int id;
        public final String videoUrl;

        public Trailer(int id, String videoUrl) {
            this.id = id;
            this.videoUrl = videoUrl;
        }

    }
}
